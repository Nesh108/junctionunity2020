﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RemoteUI_Client : TCPClient
{
    public static RemoteUI_Client Instance;

    [Header("UI References")]
    [SerializeField] private TMP_InputField IPAddress;
    [SerializeField] private Button StartClientButton;
    [SerializeField] private Transform SceneButtonParent;
    [SerializeField] private GameObject SceneButtonsPrefabs;
    [SerializeField] private TMP_Text SceneControllerSceneTitle;
    [SerializeField] private GameObject LoadingIcon;

    [Header("Views")]
    [SerializeField] private GameObject ConnectClientObject;
    [SerializeField] private GameObject SelectSceneObject;
    [SerializeField] private GameObject SceneControllerView;

    [Header("Controller View")]
    [SerializeField] private GameObject DelayedEventGroup;
    [SerializeField] private TMP_Text DelayedEventTime;
    [SerializeField] private Slider DelayedEventSlider;
    [SerializeField] private GameObject VolumeGroup;
    [SerializeField] private TMP_Text Volume;
    [SerializeField] private Slider VolumeSlider;
    [SerializeField] private GameObject BrightnessGroup;
    [SerializeField] private TMP_Text Brightness;
    [SerializeField] private Slider BrightnessSlider;

    [SerializeField] private GameObject EventCountdownGroup;
    [SerializeField] private TMP_Text EventCountdownText;

    private float _eventTriggerCountdown = -1f;

    //Set UI interactable properties
    private void Awake()
    {
        Instance = this;

        LoadingIcon.SetActive(false);
        EventCountdownGroup.SetActive(false);

        //Start Client
        StartClientButton.onClick.AddListener(ConnectToServer);

        //Populate Client delegates
        OnClientStarted = () =>
        {
            Debug.Log("Client Connected");
            ConnectClientObject.SetActive(false);
            SelectSceneObject.SetActive(true);
            SceneControllerView.SetActive(false);
            LoadingIcon.SetActive(true);
        };

        OnClientClosed = () =>
        {
            Debug.LogError("Client Closed");
            ConnectClientObject.SetActive(true);
            SelectSceneObject.SetActive(false);
            SceneControllerView.SetActive(false);
            LoadingIcon.SetActive(false);
        };

        IPAddress.text = "";
    }

    protected void ConnectToServer()
    {
        if (string.IsNullOrEmpty(IPAddress.text))
        {
            base.ipAddress = "127.0.0.1";
            base.port = 54010;
            base.StartClient();
            LoadingIcon.SetActive(true);
        }
        else if (IPAddress.text.Split(':').Length == 2)
        {
            base.ipAddress = IPAddress.text.Split(':')[0];
            base.port = int.Parse(IPAddress.text.Split(':')[1]);
            base.StartClient();
            LoadingIcon.SetActive(true);
        }
        else
        {
            Debug.LogError("Invalid IP Address");
        }
    }

    //What to do with the received message on client
    protected override void OnMessageReceived(string receivedMessage)
    {
        ClientLog("Msg recived on Client: " + "<b>" + receivedMessage + "</b>", Color.green);

        if (receivedMessage.StartsWith("SCENES="))
        {
            foreach (string sceneName in receivedMessage.Split('=')[1].Split(','))
            {
                SceneSelector sceneSelector = Instantiate(SceneButtonsPrefabs, Vector3.zero, Quaternion.identity, SceneButtonParent).GetComponent<SceneSelector>();
                sceneSelector.Scene = sceneName;
            }
            LoadingIcon.SetActive(false);
        }

        if (receivedMessage.StartsWith("SCENE_LOADED="))
        {
            SceneControllerSceneTitle.text = receivedMessage.Split('=')[1];
            GoToControllerView();
            LoadingIcon.SetActive(false);
        }
    }

    public void GoToScene(string scene)
    {
        SendMessageToServer("LOAD_SCENE=" + scene);
        LoadingIcon.SetActive(true);
    }

    public void GoToSceneSelectionView()
    {
        ConnectClientObject.SetActive(false);
        SelectSceneObject.SetActive(true);
        SceneControllerView.SetActive(false);
    }

    public void GoToControllerView()
    {
        ConnectClientObject.SetActive(false);
        SelectSceneObject.SetActive(false);
        SceneControllerView.SetActive(true);
    }

    void LateUpdate()
    {
        if (DelayedEventGroup != null && DelayedEventGroup.activeSelf)
        {
            DelayedEventTime.text = DelayedEventSlider.value + "s";
        }
        if (VolumeGroup != null && VolumeGroup.activeSelf)
        {
            Volume.text = VolumeSlider.value + "%";
        }
        if (BrightnessGroup != null && BrightnessGroup.activeSelf)
        {
            Brightness.text = BrightnessSlider.value + "%";
        }

        if (_eventTriggerCountdown >= 0f)
        {
            EventCountdownGroup.SetActive(true);
            _eventTriggerCountdown -= Time.deltaTime;
            EventCountdownText.text = $"Event in\n{_eventTriggerCountdown.ToString("F0")}s";
        }
        else
        {
            EventCountdownGroup.SetActive(false);
            _eventTriggerCountdown = -1f;
        }
    }

    public void SendDelayedEvent()
    {
        SendMessageToServer("COMMAND=DELAYED_EVENT;" + DelayedEventSlider.value.ToString("F0"));

        _eventTriggerCountdown = DelayedEventSlider.value;
    }

    public void SendInstantEvent()
    {
        SendMessageToServer("COMMAND=INSTANT_EVENT");
    }

    public void SendPauseEvent()
    {
        SendMessageToServer("COMMAND=PAUSE");
    }

    public void SendRestartGameEvent()
    {
        SendMessageToServer("COMMAND=RESTART");
    }

    public void SendEndGameEvent()
    {
        SendMessageToServer("COMMAND=END_GAME");
    }

    public void SendMute()
    {
        SendMessageToServer("COMMAND=VOLUME_STATE;false");
    }

    public void SendUnmute()
    {
        SendMessageToServer("COMMAND=VOLUME_STATE;true");
    }

    public void SendVolumeLevel()
    {
        SendMessageToServer("COMMAND=VOLUME_LEVEL;" + VolumeSlider.value.ToString("F0"));
    }

    public void SendBrightnessLevel()
    {
        SendMessageToServer("COMMAND=BRIGHTNESS_LEVEL;" + BrightnessSlider.value.ToString("F0"));
    }

}

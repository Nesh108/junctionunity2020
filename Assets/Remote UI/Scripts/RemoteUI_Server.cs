﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RemoteUI_Server : TCPServer
{
    public static RemoteUI_Server Instance;

    [SerializeField] private GameObject ConnectedClientIcon;

    void Awake()
    {
        Instance = this;
    }

    //Set UI interactable properties
    void OnEnable()
    {
        ConnectedClientIcon.SetActive(false);
        StartServer();

        //Populate Server delegates
        OnClientConnected = () =>
        {
            Debug.Log("CLIENT CONNECTED");
        };

        OnServerClosed = () =>
        {
            Debug.Log("CLOSE SERVER HAPPENED");
            StartServer();
        };
    }

    void OnDisable()
    {
        Debug.Log("Closing Server");
        CloseServer();
    }

    //What to do with the received message on server
    protected override void OnMessageReceived(string receivedMessage)
    {
        ServerLog("Msg recived on Server: " + "<b>" + receivedMessage + "</b>", Color.green);
        switch (receivedMessage)
        {
            case "Close":
                //In this case we send "Close" to shut down client
                SendMessageToClient("Close");
                //Close client connection
                CloseClientConnection();
                break;
            case "Scenes":
                Debug.Log("Scenes List requested. Sending...");
                SendMessageToClient("SCENES=" + GetScenesList());
                break;
        }

        if (receivedMessage.StartsWith("LOAD_SCENE="))
        {
            string sceneName = receivedMessage.Split('=')[1];
            RemoteUIDispatcher.Instance.BroadcastSelectScene(sceneName);
            SendMessageToClient("SCENE_LOADED=" + sceneName);
        }

        if (receivedMessage.StartsWith("COMMAND="))
        {
            string[] commands = receivedMessage.Split('=')[1].Split(';');
            switch (commands[0])
            {
                case "DELAYED_EVENT":
                    RemoteUIDispatcher.Instance.BroadcastDelayedPerformEvent(float.Parse(commands[1]));
                    break;
                case "INSTANT_EVENT":
                    RemoteUIDispatcher.Instance.BroadcastInstantEvent();
                    break;
                case "PAUSE":
                    RemoteUIDispatcher.Instance.BroadcastPause();
                    break;
                case "VOLUME_STATE":
                    RemoteUIDispatcher.Instance.BroadcastVolumeState(bool.Parse(commands[1]));
                    break;
                case "VOLUME_LEVEL":
                    RemoteUIDispatcher.Instance.BroadcastVolumeLevel(float.Parse(commands[1]));
                    break;
                case "RESTART":
                    RemoteUIDispatcher.Instance.BroadcastRestart();
                    break;
                case "BRIGHTNESS_LEVEL":
                    RemoteUIDispatcher.Instance.BroadcastBrightnessLevel(float.Parse(commands[1]));
                    break;
                case "END_GAME":
                    RemoteUIDispatcher.Instance.BroadcastEndGame();
                    break;
                default:
                    Debug.LogError($"Cannot understand {commands[0]}");
                    break;
            }
        }
    }

    private string GetScenesList()
    {
        string sceneNames = "";
        for (int i = 3; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            if (!SceneUtility.GetScenePathByBuildIndex(i).Contains("EndLevel"))
            {
                if (i > 3)
                {
                    sceneNames += ",";
                }

                sceneNames += System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
            }
        }

        return sceneNames;
    }

    void LateUpdate()
    {
        if (ConnectedClientIcon != null)
        {
            if (IsClientConnected())
            {
                ConnectedClientIcon.SetActive(true);
            }
            else
            {
                ConnectedClientIcon.SetActive(false);
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class NetworkModeSelector : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
            Debug.Log("Starting as Admin");
            SceneManager.LoadScene("Admin_Loader");
        }
        else if (Input.GetKeyUp(KeyCode.F2))
        {
            Debug.Log("Starting as Patient");
            SceneManager.LoadScene("Patient_Loader");
        }

    }
}

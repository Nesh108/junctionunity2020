﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class RemoteSceneSelector : MonoBehaviour
{

    void OnEnable()
    {
        EventManager.StartListening(RemoteUIDispatcher.ON_SELECT_SCENE, OnSelectSceneEvent);
    }

    void OnDisable()
    {
        EventManager.StopListening(RemoteUIDispatcher.ON_SELECT_SCENE, OnSelectSceneEvent);
    }

    private void OnSelectSceneEvent(EventParam eventParam)
    {
        Debug.Log($"Remotely selecting {eventParam.string_param}");
        SceneManager.LoadScene(eventParam.string_param);
    }
}

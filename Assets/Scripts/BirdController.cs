﻿using UnityEngine;

public class BirdController : MonoBehaviour
{
    [SerializeField] private GameObject[] birds;
    [SerializeField] private GameObject[] waypoints;

    void Start()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().setWaypoints(waypoints);
        }
    }
    public void flyIn()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().flyIn();
        }
    }

    public void flyOut()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().flyOut();
        }
    }
}

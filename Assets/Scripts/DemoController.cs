﻿using UnityEngine;
using System.Collections;

public class DemoController : MonoBehaviour
{
    private bool flyingIn = false;
    [SerializeField] private GameObject[] birds;
    [SerializeField] private GameObject[] waypoints;
    
    void Start()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().setWaypoints(waypoints);
        }
    }
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (flyingIn)
                crazyEventEnd();
            else
                crazyEventStart();

            flyingIn = !flyingIn;
            Debug.Log("Flying direction: " + (flyingIn ? "in" : "out"));
        }

    }

    public void flyIn()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().flyIn();
        }
    }

    public void flyOut()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().flyOut();
        }
    }

    public void crazyEventStart()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().setCrazy(true);
            bird.GetComponent<FlyAndFollowPath>().flyIn();
        }
    }

    public void crazyEventEnd()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().setCrazy(false);
            bird.GetComponent<FlyAndFollowPath>().flyOut();
        }
    }
}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RemoteLevelController : MonoBehaviour
{
    [SerializeField] private Light LevelLight;
    [SerializeField] [Range(0f, 3f)] private float MaxBrightness;
    [SerializeField] private string EndGameSceneName;
    [SerializeField] private GameObject BonusLevelObject;
    [SerializeField] private GameObject[] NormalLevelObjects;

    void OnEnable()
    {
        EventManager.StartListening(RemoteUIDispatcher.ON_PAUSE_SCENE, OnPauseScene);
        EventManager.StartListening(RemoteUIDispatcher.ON_VOLUME_STATE_CHANGED, OnVolumeStateChanged);
        EventManager.StartListening(RemoteUIDispatcher.ON_RESTART_SCENE, OnrRestartScene);
        EventManager.StartListening(RemoteUIDispatcher.ON_VOLUME_LEVEL_CHANGED, OnVolumeLevelChanged);
        EventManager.StartListening(RemoteUIDispatcher.ON_BRIGHTNESS_LEVEL_CHANGED, OnBrightnessLevelChanged);
        EventManager.StartListening(RemoteUIDispatcher.ON_END_GAME, OnEndGame);
        EventManager.StartListening(RemoteUIDispatcher.ON_DELAYED_PERFORM_EVENT, OnDelayedBonus);
        EventManager.StartListening(RemoteUIDispatcher.ON_INSTANT_EVENT, OnInstantBonus);
    }

    void OnDisable()
    {
        EventManager.StopListening(RemoteUIDispatcher.ON_PAUSE_SCENE, OnPauseScene);
        EventManager.StopListening(RemoteUIDispatcher.ON_VOLUME_STATE_CHANGED, OnVolumeStateChanged);
        EventManager.StopListening(RemoteUIDispatcher.ON_RESTART_SCENE, OnrRestartScene);
        EventManager.StopListening(RemoteUIDispatcher.ON_VOLUME_LEVEL_CHANGED, OnVolumeLevelChanged);
        EventManager.StopListening(RemoteUIDispatcher.ON_BRIGHTNESS_LEVEL_CHANGED, OnBrightnessLevelChanged);
        EventManager.StopListening(RemoteUIDispatcher.ON_END_GAME, OnEndGame);
        EventManager.StopListening(RemoteUIDispatcher.ON_DELAYED_PERFORM_EVENT, OnDelayedBonus);
        EventManager.StopListening(RemoteUIDispatcher.ON_INSTANT_EVENT, OnInstantBonus);
    }

    private void OnPauseScene(EventParam eventParam)
    {
        Debug.Log("Setting Paused:" + (Time.timeScale > 0.5f));
        Time.timeScale = Time.timeScale < 1f ? 1f : 0.2f;
    }

    private void OnVolumeStateChanged(EventParam eventParam)
    {
        Debug.Log("Setting Mute: " + !eventParam.bool_param);
        AudioListener.volume = eventParam.bool_param ? 1f : 0f;
    }

    private void OnrRestartScene(EventParam eventParam)
    {
        Debug.Log("Reloading Scene");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnVolumeLevelChanged(EventParam eventParam)
    {
        Debug.Log($"Setting new volume to: {eventParam.float_param / 100f}");
        AudioListener.volume = eventParam.float_param / 100f;
    }

    private void OnBrightnessLevelChanged(EventParam eventParam)
    {
        Debug.Log($"Setting brightness level to: {eventParam.float_param / 100f * MaxBrightness}");
        LevelLight.intensity = eventParam.float_param / 100f * MaxBrightness;
    }

    private void OnEndGame(EventParam eventParam)
    {
        Debug.Log("Loading End Game Scene");
        SceneManager.LoadScene(EndGameSceneName);
    }

    private void OnDelayedBonus(EventParam eventParam)
    {
        StartCoroutine(StartBonus(eventParam.float_param));
    }

    private void OnInstantBonus(EventParam eventParam)
    {
        StartCoroutine(StartBonus(0f));
    }

    IEnumerator StartBonus(float t)
    {
        yield return new WaitForSeconds(t);
        foreach (GameObject g in NormalLevelObjects)
        {
            g.SetActive(false);
        }

        BonusLevelObject.SetActive(true);
    }

}

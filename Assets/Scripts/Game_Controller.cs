﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Game_Controller : MonoBehaviour
{
    [SerializeField] private string LevelName = "";
    [SerializeField] private AudioSource GameAudioSource;
    [SerializeField] private VoiceInputDetector VoiceInputDetectorScript;

    [Header("Level")]
    [SerializeField] private UnityEvent[] Round1_Actions;
    [SerializeField] private float DelayBeforeRoundStarts = 5f;     // Wait X seconds before starting the level
    [SerializeField] private AudioClip[] Audios;
    [SerializeField] private float[] PauseBetweenAudios;
    [SerializeField] private bool[] WaitForUserInput;

    [SerializeField] private GameObject NextLevel;
    [SerializeField] private bool DisableObjectOnFinish = true;

    void OnEnable()
    {
        StartCoroutine(GameDirectorScript());

        if (Round1_Actions.Length != Audios.Length || Round1_Actions.Length != PauseBetweenAudios.Length ||
            Round1_Actions.Length != WaitForUserInput.Length)
        {
            Debug.LogError($"Missing Data in {LevelName} object");
        }
    }

    IEnumerator GameDirectorScript()
    {
        yield return new WaitForSeconds(DelayBeforeRoundStarts);

        Debug.Log($"Starting {LevelName}");
        for (int i = 0; i < Audios.Length; i++)
        {
            GameAudioSource.PlayOneShot(Audios[i]);

            if (Round1_Actions[i] != null)
            {
                Round1_Actions[i].Invoke();
            }

            yield return new WaitForSeconds(Audios[i].length);

            if (WaitForUserInput[i])
            {
                VoiceInputDetectorScript.SetDetectVoiceState(true);

                Debug.Log("Waiting for user voice input...");
                yield return new WaitUntil(() => { return VoiceInputDetectorScript.IsVoiceDetected; });
                Debug.Log("User voice detected!");
            }

            VoiceInputDetectorScript.SetDetectVoiceState(false);

            yield return new WaitForSeconds(PauseBetweenAudios[i]);
        }
        Debug.Log($"{LevelName} Over");

        if (NextLevel != null)
        {
            NextLevel.SetActive(true);

            if (DisableObjectOnFinish)
            {
                gameObject.SetActive(false);
            }
        }
    }
}

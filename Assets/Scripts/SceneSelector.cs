﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SceneSelector : MonoBehaviour
{

    [SerializeField] private Button SceneButton;
    [SerializeField] private TMP_Text SceneButtonText;

    public string Scene;

    // Start is called before the first frame update
    void Start()
    {
        SceneButton.gameObject.SetActive(true);
        SceneButton.onClick.AddListener(GoToScene);
        SceneButtonText.text = Scene;
    }

    void GoToScene()
    {
        RemoteUI_Client.Instance.GoToScene(Scene);
    }
}

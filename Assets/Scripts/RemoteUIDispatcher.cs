﻿using UnityEngine;

public class RemoteUIDispatcher : MonoBehaviour
{
    public static RemoteUIDispatcher Instance = null;

    public const string ON_DELAYED_PERFORM_EVENT = "OnDelayedPerformEvent";
    public const string ON_INSTANT_EVENT = "OnInstantEvent";
    public const string ON_SELECT_SCENE = "OnSelectScene";
    public const string ON_PAUSE_SCENE = "OnPauseScene";
    public const string ON_RESTART_SCENE = "OnRestartScene";
    public const string ON_END_GAME = "OnEndGame";
    public const string ON_VOLUME_STATE_CHANGED = "OnVolumeStateChanged";
    public const string ON_VOLUME_LEVEL_CHANGED = "OnVolumeLevelChanged";
    public const string ON_BRIGHTNESS_LEVEL_CHANGED = "OnBrightnessLevelChanged";

    void Awake()
    {
        Instance = this;
    }

    public void BroadcastSelectScene(string n)
    {
        Debug.Log("Broadcasting SelectScene");
        EventParam brcastParam = new EventParam
        {
            string_param = n
        };
        EventManager.TriggerEvent(ON_SELECT_SCENE, brcastParam);
    }

    public void BroadcastDelayedPerformEvent(float t)
    {
        Debug.Log("Broadcasting DelayedPerformEvent");
        EventParam brcastParam = new EventParam
        {
            float_param = t
        };
        EventManager.TriggerEvent(ON_DELAYED_PERFORM_EVENT, brcastParam);
    }

    public void BroadcastInstantEvent()
    {
        Debug.Log("Broadcasting InstantEvent");
        EventParam brcastParam = new EventParam
        {
        };
        EventManager.TriggerEvent(ON_INSTANT_EVENT, brcastParam);
    }

    public void BroadcastPause()
    {
        Debug.Log("Broadcasting Pause");
        EventParam brcastParam = new EventParam
        {
        };
        EventManager.TriggerEvent(ON_PAUSE_SCENE, brcastParam);
    }

    public void BroadcastRestart()
    {
        Debug.Log("Broadcasting Restart");
        EventParam brcastParam = new EventParam
        {
        };
        EventManager.TriggerEvent(ON_RESTART_SCENE, brcastParam);
    }

    public void BroadcastEndGame()
    {
        Debug.Log("Broadcasting End Game");
        EventParam brcastParam = new EventParam
        {
        };
        EventManager.TriggerEvent(ON_END_GAME, brcastParam);
    }

    public void BroadcastVolumeState(bool isMuted)
    {
        Debug.Log("Broadcasting Volume State: " + isMuted);
        EventParam brcastParam = new EventParam
        {
            bool_param = isMuted
        };
        EventManager.TriggerEvent(ON_VOLUME_STATE_CHANGED, brcastParam);
    }

    public void BroadcastVolumeLevel(float level)
    {
        Debug.Log("Broadcasting Volume Level: " + level);
        EventParam brcastParam = new EventParam
        {
            float_param = level
        };
        EventManager.TriggerEvent(ON_VOLUME_LEVEL_CHANGED, brcastParam);
    }

    public void BroadcastBrightnessLevel(float level)
    {
        Debug.Log("Broadcasting Brightness Level: " + level);
        EventParam brcastParam = new EventParam
        {
            float_param = level
        };
        EventManager.TriggerEvent(ON_BRIGHTNESS_LEVEL_CHANGED, brcastParam);
    }
}

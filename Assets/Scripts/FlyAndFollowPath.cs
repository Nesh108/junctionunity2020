﻿using UnityEngine;

public class FlyAndFollowPath : MonoBehaviour
{
    public float speed = 1f;
    private GameObject[] m_waypoints;
    private Transform waypointOutOfView;
    private int currentWaypoint = 0;
    private Transform targetWaypoint;
    private AudioSource m_audioSource;

    private bool flying = false;
    private bool flyingIn = false;
    private bool isCrazy = false;

    // Start is called before the first frame update
    void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
        if (m_audioSource != null)
        {
            m_audioSource.loop = true;
        }

        waypointOutOfView = GameObject.Find("WaypointOutOfView").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (flying)
        {
            if (targetWaypoint == null)
                targetWaypoint = m_waypoints[0].transform;

            rotateAndFlyTowardsTarget();
        }

    }

    void rotateAndFlyTowardsTarget()
    {
        transform.forward = Vector3.RotateTowards(transform.forward, targetWaypoint.position - transform.position, speed * Time.deltaTime, 0f);
        transform.position = Vector3.MoveTowards(transform.position, targetWaypoint.position, speed * Time.deltaTime);

        if (transform.position == targetWaypoint.position)
        {
            if (flyingIn)
            {
                if (currentWaypoint == m_waypoints.Length - 1)
                {
                    currentWaypoint = 0;
                }
                else
                {
                    if (isCrazy)
                        currentWaypoint = Random.Range(0, m_waypoints.Length);
                    else
                        currentWaypoint++;
                }

                targetWaypoint = m_waypoints[currentWaypoint].transform;
            }
            else
            {
                flying = false;
                targetWaypoint = null;
            }
        }
    }

    public void flyIn()
    {
        flyingIn = true;
        if (!flying)
        {
            flying = true;
            targetWaypoint = m_waypoints[0].transform;
        }

        if (m_audioSource != null)
        {
            m_audioSource.Play();
        }
    }

    public void flyOut()
    {
        flyingIn = false;
        if (flying)
            targetWaypoint = waypointOutOfView;

        if (m_audioSource != null)
        {
            m_audioSource.Stop();
        }
    }

    public void setWaypoints(GameObject[] waypoints)
    {
        m_waypoints = waypoints;
    }

    public void setCrazy(bool crazy)
    {
        isCrazy = crazy;
    }
}

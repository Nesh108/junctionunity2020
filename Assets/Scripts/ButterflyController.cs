﻿using UnityEngine;

public class ButterflyController : MonoBehaviour
{
    [SerializeField] private GameObject[] birds;
    [SerializeField] private GameObject[] crazyBirds;
    [SerializeField] private GameObject[] waypoints;
    [SerializeField] private GameObject[] crazyWaypoints;
    void Start()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().setWaypoints(waypoints);
        }
        foreach (GameObject bird in crazyBirds)
        {
            bird.GetComponent<FlyAndFollowPath>().setWaypoints(crazyWaypoints);
        }
    }
    public void flyIn()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().flyIn();
        }
    }

    public void flyOut()
    {
        foreach (GameObject bird in birds)
        {
            bird.GetComponent<FlyAndFollowPath>().flyOut();
        }
    }
    public void crazyEventStart()
    {
        foreach (GameObject bird in crazyBirds)
        {
            bird.GetComponent<FlyAndFollowPath>().setCrazy(true);
            bird.GetComponent<FlyAndFollowPath>().flyIn();
        }
    }

    public void crazyEventEnd()
    {
        foreach (GameObject bird in crazyBirds)
        {
            bird.GetComponent<FlyAndFollowPath>().setCrazy(false);
            bird.GetComponent<FlyAndFollowPath>().flyOut();
        }
    }
}

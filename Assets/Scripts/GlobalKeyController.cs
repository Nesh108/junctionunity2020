﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalKeyController : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene("NetworkSelector");
            if (RemoteUI_Client.Instance != null && RemoteUI_Client.Instance.IsClientActive())
            {
                RemoteUI_Client.Instance.CloseClient();
            }
            if (RemoteUI_Server.Instance != null && RemoteUI_Server.Instance.IsServerActive())
            {
                RemoteUI_Server.Instance.CloseServer();
            }
        }
    }
}

﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Kulplex.Core
{
    public static class Utils
    {
        public static GameObject GetPersistentGameObject(string withName, bool showWarning = true)
        {
            foreach (PersistentObject go in GameObject.FindObjectsOfType<PersistentObject>())
            {
                if (go.name.Equals(withName, StringComparison.Ordinal))
                {
                    return go.gameObject;
                }
            }

            if (showWarning)
            {
                Debug.LogWarning("Cannot find GameObject with name: " + withName);
            }

            return null;
        }

        public static GameObject GetPersistentGameObjectByTag(string withTag, bool showWarning = true)
        {
            foreach (PersistentObject go in GameObject.FindObjectsOfType<PersistentObject>())
            {
                if (go.CompareTag(withTag))
                {
                    return go.gameObject;
                }
            }

            if (showWarning)
            {
                Debug.LogWarning("Cannot find GameObject with tag: " + withTag);
            }

            return null;
        }

        public static GameObject GetRootGameObjectFromSpecificScene(string sceneName, string withName, bool showWarning = true)
        {
            foreach (GameObject go in SceneManager.GetSceneByName(sceneName).GetRootGameObjects())
            {
                if (go.name.Equals(withName, StringComparison.Ordinal))
                {
                    return go;
                }
            }

            if (showWarning)
            {
                Debug.LogWarning("Cannot find GameObject with name: " + withName);
            }

            return null;
        }

        public static GameObject GetRootGameObjectFromSpecificSceneByTag(string sceneName, string withTag, bool showWarning = true)
        {
            foreach (GameObject go in SceneManager.GetSceneByName(sceneName).GetRootGameObjects())
            {
                if (go.CompareTag(withTag))
                {
                    return go;
                }
            }

            if (showWarning)
            {
                Debug.LogWarning("Cannot find GameObject with tag: " + withTag);
            }

            return null;
        }

        public static GameObject GetChildGameObject(GameObject fromGameObject, string withName, bool showWarning = true)
        {
            //Author: Isaac Dart, June-13.
            try
            {
                Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(true);
                foreach (Transform t in ts)
                {
                    if (t.gameObject.name.Equals(withName, StringComparison.Ordinal))
                    {
                        return t.gameObject;
                    }
                }
            }
            catch (Exception)
            {
            }

            if (showWarning)
            {
                Debug.LogWarning("Cannot find GameObject with name: " + withName);
            }

            return null;
        }

        public static GameObject GetChildGameObjectByTag(GameObject fromGameObject, string withTag, bool showWarning = true)
        {
            //Author: Isaac Dart, June-13.
            try
            {
                Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform>(true);
                foreach (Transform t in ts)
                {
                    if (t.gameObject.CompareTag(withTag))
                    {
                        return t.gameObject;
                    }
                }
            }
            catch (Exception)
            {
            }

            if (showWarning)
            {
                Debug.LogWarning("Cannot find GameObject with tag: " + withTag);
            }
            return null;
        }

        public static void SetGameObjectAsChild(GameObject parent, GameObject child, bool keepCoordinates,
            bool worldStays = true)
        {
            child.transform.SetParent(parent.transform, worldStays);
            if (!keepCoordinates)
            {
                child.transform.localRotation = Quaternion.identity;
                child.transform.localPosition = Vector3.zero;
                child.transform.localScale = new Vector3(Math.Abs(child.transform.localScale.x),
                    Math.Abs(child.transform.localScale.y), Math.Abs(child.transform.localScale.z));
            }
        }

        public static void UnsetGameObjectAsChild(GameObject go, Transform parent)
        {
            go.transform.SetParent(parent);
        }

        public static void SetCameraCullingLayer(Camera cam, string layerName, CameraCullingLayerAction action)
        {
            if (action.Equals(CameraCullingLayerAction.SHOW))
            {
                cam.cullingMask |= 1 << LayerMask.NameToLayer(layerName);
            }
            else
            {
                cam.cullingMask &= ~(1 << LayerMask.NameToLayer(layerName));
            }
        }

        public static int GetSceneIdByName(string sceneName)
        {
            for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
            {
                string scenePath = SceneUtility.GetScenePathByBuildIndex(i);
                if (scenePath != null && Path.GetFileNameWithoutExtension(scenePath)
                        .Equals(sceneName, StringComparison.Ordinal))
                {
                    return i;
                }
            }

            return -1;
        }

        public static T GetComponentIfNull<T>(this Component that, T cachedT, bool showWarnings = true)
            where T : Component
        {
            if (cachedT == null)
            {
                if (showWarnings)
                {
                    Debug.LogWarning("Looking for type " + typeof(T) + " on " + that.name, that);
                }

                cachedT = (T)that.GetComponent(typeof(T));
                if (cachedT == null && showWarnings)
                {
                    Debug.LogWarning("GetComponent of type " + typeof(T) + " failed on " + that.name, that);
                }
            }

            return cachedT;
        }

        public static T GetComponentIfNull<T>(this GameObject that, T cachedT, bool showWarnings = true)
            where T : Component
        {
            if (cachedT == null)
            {
                if (showWarnings)
                {
                    Debug.LogWarning("Looking for type " + typeof(T) + " on " + that.name, that);
                }

                cachedT = (T)that.GetComponent(typeof(T));
                if (cachedT == null && showWarnings)
                {
                    Debug.LogWarning("GetComponent of type " + typeof(T) + " failed on " + that.name, that);
                }
            }

            return cachedT;
        }

        public static T GetComponentInChildrenIfNull<T>(this Component that, T cachedT, bool showWarnings = true)
            where T : Component
        {
            if (cachedT == null)
            {
                if (showWarnings)
                {
                    Debug.LogWarning("Looking for type " + typeof(T) + " on " + that.name, that);
                }

                cachedT = (T)that.GetComponentInChildren(typeof(T));
                if (cachedT == null && showWarnings)
                {
                    Debug.LogWarning("GetComponentInChildren of type " + typeof(T) + " failed on " + that.name, that);
                }
            }

            return cachedT;
        }

        public static T GetComponentInChildrenIfNull<T>(this GameObject that, T cachedT, bool showWarnings = true)
            where T : Component
        {
            if (cachedT == null)
            {
                if (showWarnings)
                {
                    Debug.LogWarning("Looking for type " + typeof(T) + " on " + that.name, that);
                }

                cachedT = (T)that.GetComponentInChildren(typeof(T));
                if (cachedT == null && showWarnings)
                {
                    Debug.LogWarning("GetComponentInChildren of type " + typeof(T) + " failed on " + that.name, that);
                }
            }

            return cachedT;
        }

        public static float GetAnimationLength(this Animator that, string animationName, StringComparison sc = StringComparison.InvariantCultureIgnoreCase)
        {
            RuntimeAnimatorController ac = that.runtimeAnimatorController;    //Get Animator controller
            for (int i = 0; i < ac.animationClips.Length; i++)                 //For all animations
            {
                if (ac.animationClips[i].name.Equals(animationName, sc))        //If it has the same name as your clip
                {
                    return ac.animationClips[i].length;
                }
            }

            Debug.LogWarning("GetAnimationLength couldn't find animation " + animationName);
            return -1f;
        }
    }
}

public enum CameraCullingLayerAction
{
    SHOW,
    HIDE
}
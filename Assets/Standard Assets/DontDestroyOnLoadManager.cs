﻿using System.Collections.Generic;
using UnityEngine;

public static class DontDestroyOnLoadManager
{
    static List<GameObject> _ddolObjects = new List<GameObject>();

    public static void DontDestroyOnLoad(this GameObject go)
    {
        if (!_ddolObjects.Contains(go))
        {
            Debug.Log("DontDestroyOnLoad Added: " + go.name);
            Object.DontDestroyOnLoad(go);
            _ddolObjects.Add(go);
        }
        else
        {
            Debug.Log("DontDestroyOnLoad NOT Added [Duplicate]: " + go.name);
        }
    }

    public static void DestroyAll(bool skipCamera = false)
    {
        foreach (var go in _ddolObjects)
        {
            if (go != null && !(skipCamera && go.name.Equals("Main Camera", System.StringComparison.Ordinal)))
            {
                Object.Destroy(go);
            }
        }

        _ddolObjects.Clear();
        if (skipCamera)
        {
            _ddolObjects.Add(Camera.main.gameObject);
        }
    }

    public static GameObject FindDontDestroyGameObjectByName(string name)
    {
        foreach (GameObject go in _ddolObjects)
        {
            if (go != null && go.name.Equals(name, System.StringComparison.Ordinal))
            {
                return go;
            }
        }

        return null;
    }

    public static GameObject FindDontDestroyGameObjectByTag(string tag)
    {
        foreach (GameObject go in _ddolObjects)
        {
            if (go.CompareTag(tag))
            {
                return go;
            }
        }

        return null;
    }
}
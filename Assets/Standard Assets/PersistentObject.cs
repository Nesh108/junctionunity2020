﻿using UnityEngine;

public class PersistentObject : MonoBehaviour
{
    public string Name;

    private void OnEnable()
    {
        GameObject prevGo = DontDestroyOnLoadManager.FindDontDestroyGameObjectByName(name);
        if (prevGo != null && prevGo.GetHashCode() != GetHashCode())
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoadManager.DontDestroyOnLoad(gameObject);
        Name = name;
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

public class AudioDetector : MonoBehaviour
{
    [SerializeField] private float SoundThreshold = -60f;
    [SerializeField] private Image VoiceNotDetected;

    private string _device;
    private AudioClip _clipRecord;
    private bool _isInitialized;
    private float _currentAudioLevel;

    void Start()
    {
        if (_device == null)
        {
            _device = Microphone.devices[0];
        }
        _clipRecord = Microphone.Start(_device, true, 999, 44100);
        _isInitialized = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isInitialized)
        {
            _currentAudioLevel = GetMaximumAudioLevel();

            VoiceNotDetected.enabled = _currentAudioLevel < SoundThreshold;
        }
    }

    public float GetMaximumAudioLevel()
    {
        int _sampleWindow = 128;
        float levelMax = 0;
        float[] waveData = new float[_sampleWindow];
        int micPosition = Microphone.GetPosition(null) - (_sampleWindow + 1); // null means the first microphone
        if (micPosition < 0) return 0;
        _clipRecord.GetData(waveData, micPosition);
        // Getting a peak on the last 128 samples
        for (int i = 0; i < _sampleWindow; i++)
        {
            float wavePeak = waveData[i] * waveData[i];
            if (levelMax < wavePeak)
            {
                levelMax = wavePeak;
            }
        }
        float db = 20 * Mathf.Log10(Mathf.Abs(levelMax));
        return db;
    }
}
